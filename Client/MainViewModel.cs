﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using Client.Annotations;

namespace Client
{
    public class ItemModel : INotifyPropertyChanged
    {
        private string _name;
        private int _value;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }
    }

    public class MainViewModel
    {
        public ObservableCollection<ItemModel> Items { get; set; }

        public MainViewModel()
        {
            Items = new ObservableCollection<ItemModel>();

            Items.Add(new ItemModel
            {
                Name = "Item1",
                Value = 100
            });
            Items.Add(new ItemModel
            {
                Name = "Item2",
                Value = 101
            });
            Items.Add(new ItemModel
            {
                Name = "Item3",
                Value = 102
            });
        }

        public void UpdateItemTwo()
        {
            Items[1].Value = 400;
        }
    }
}
